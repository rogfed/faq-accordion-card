module.exports = {
  root: true,
  extends: [ 'eslint:recommended', 'plugin:@typescript-eslint/recommended', 'plugin:react/recommended', 'plugin:react/jsx-runtime', 'plugin:react-hooks/recommended' ],
  plugins: [ '@typescript-eslint', 'react' ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    ecmaFeatures: { jsx: true }
  },
  env: {
    browser: true,
    node: true
  },
  rules: {
    'array-bracket-newline': [ 'warn', { 'multiline': true } ],
    'array-bracket-spacing': [ 'warn', 'always' ],
    'array-element-newline': [ 'warn', 'consistent' ],
    'arrow-parens': [ 'warn', 'as-needed' ],
    'arrow-spacing': [
      'warn', {
        'before': true,
        'after': true
      }
    ],
    'block-spacing': [ 'warn', 'always' ],
    'brace-style': [ 'warn', '1tbs' ],
    'comma-dangle': [ 'warn', 'never' ],
    'comma-spacing': [
      'warn', {
        before: false,
        after: true
      }
    ],
    'comma-style': [ 'warn', 'last' ],
    'computed-property-spacing': [ 'warn', 'never' ],
    'dot-location': [ 'warn', 'object' ],
    'eol-last': [ 'warn', 'always' ],
    'func-call-spacing': [ 'warn', 'never' ],
    'function-call-argument-newline': [ 'warn', 'consistent' ],
    'function-paren-newline': [ 'warn', 'multiline' ],
    'implicit-arrow-linebreak': [ 'warn', 'beside' ],
    'indent': [ 'warn', 2 ],
    'jsx-quotes': [ 'warn', 'prefer-double' ],
    'key-spacing': [ 'warn', { 'mode': 'strict' } ],
    'keyword-spacing': [
      'warn', {
        'before': true,
        after: true
      }
    ],
    'linebreak-style': [ 'warn', 'unix' ],
    'lines-around-comment': [
      'warn', {
        'beforeBlockComment': true,
        'beforeLineComment': true,
        'allowBlockStart': true,
        'allowObjectStart': true,
        'allowArrayStart': true,
        'allowClassStart': true
      }
    ],
    'lines-between-class-members': [ 'warn', 'always' ],
    'multiline-ternary': [ 'warn', 'always-multiline' ],
    'new-parens': [ 'warn', 'never' ],
    'newline-per-chained-call': [ 'warn', { 'ignoreChainWithDepth': 2 } ],
    'no-extra-parens': [ 'warn', 'all', { ignoreJSX: 'multi-line' } ],
    'no-multi-spaces': [ 'warn', { ignoreEOLComments: false } ],
    'no-multiple-empty-lines': [ 'warn', { max: 1 } ],
    'no-trailing-spaces': 'warn',
    'no-whitespace-before-property': 'warn',
    'nonblock-statement-body-position': [ 'warn', 'below' ],
    'object-curly-newline': [ 'warn', { 'multiline': true } ],
    'object-curly-spacing': [ 'warn', 'always' ],
    'object-property-newline': 'warn',
    'operator-linebreak': [ 'warn', 'none' ],
    'padded-blocks': [ 'warn', 'never' ],
    'padding-line-between-statements': [
      'warn', {
        'blankLine': 'any',
        'prev': '*',
        'next': '*'
      }
    ],
    'quotes': [ 'warn', 'single' ],
    'rest-spread-spacing': [ 'warn', 'never' ],
    'semi': [ 'warn', 'always' ],
    'semi-spacing': [
      'warn', {
        'before': false,
        'after': true
      }
    ],
    'semi-style': [ 'warn', 'last' ],
    'space-before-blocks': [
      'warn', {
        'functions': 'never',
        'keywords': 'always',
        'classes': 'always'
      }
    ],
    'space-before-function-paren': [ 'warn', 'never' ],
    'space-in-parens': [ 'warn', 'never' ],
    'space-infix-ops': [ 'warn', { 'int32Hint': false } ],
    'space-unary-ops': [
      'warn', {
        'words': true,
        'nonwords': false
      }
    ],
    'switch-colon-spacing': [
      'warn', {
        'after': true,
        'before': false
      }
    ],
    'template-curly-spacing': [ 'warn', 'never' ],
    'template-tag-spacing': [ 'warn', 'never' ],
    'unicode-bom': [ 'warn', 'never' ],
    'wrap-iife': [ 'warn', 'outside' ],
    'wrap-regex': 'warn',
    'yield-star-spacing': [ 'warn', 'after' ]
  }
};
